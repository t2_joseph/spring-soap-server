package com.padippurackal.web.service;

import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.ws.config.annotation.EnableWs;
import org.springframework.ws.config.annotation.WsConfigurerAdapter;
import org.springframework.ws.transport.http.MessageDispatcherServlet;
import org.springframework.ws.wsdl.wsdl11.DefaultWsdl11Definition;
import org.springframework.xml.xsd.SimpleXsdSchema;
import org.springframework.xml.xsd.XsdSchema;

@EnableWs
@Configuration
public class WebServiceConfig extends WsConfigurerAdapter {

    //Spring WS uses a different servlet type for handling SOAP messages - MessageDispatcherServlet
    //ApplicationContext needs to be injected to MessageDispatcherServlet for Spring
    //to detect Spring Beans.
    @Bean
    public ServletRegistrationBean messageDispatcherServlet(ApplicationContext applicationContext) {
        MessageDispatcherServlet messageDispatcherServlet = new MessageDispatcherServlet();
        messageDispatcherServlet.setApplicationContext(applicationContext);
        //Uses WSDL location servlet transformation so that soap:address will have correct ip & address.
        messageDispatcherServlet.setTransformWsdlLocations(true);
        return new ServletRegistrationBean(messageDispatcherServlet, "/ws/*");
    }

    //DefaultWsdl11Definition exposes a standard WSDL 1.1 using XsdSchema
    //Bean names determine the URL under which web service and the generated WSDL file is available.
    //In this case, the WSDL will be available under http://<host>:<port>/ws/books.wsdl
    @Bean(name="books")
    public DefaultWsdl11Definition defaultWsdl11Definition(XsdSchema booksSchema){
        DefaultWsdl11Definition defaultWsdl11Definition = new DefaultWsdl11Definition();
        defaultWsdl11Definition.setPortTypeName("BooksPort");
        defaultWsdl11Definition.setLocationUri("/ws");
        defaultWsdl11Definition.setTargetNamespace("http://web.padippurackal.com/service/model");
        defaultWsdl11Definition.setSchema(booksSchema);
        return defaultWsdl11Definition;
    }

    @Bean
    public XsdSchema booksSchema() {
        return new SimpleXsdSchema(new ClassPathResource("books.xsd"));
    }
}
