package com.padippurackal.web.service;

import com.padippurackal.web.service.model.BookRequest;

import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.ClassUtils;
import org.springframework.ws.client.core.WebServiceTemplate;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class SpringSoapServerApplicationIntegrationTests {

	private Jaxb2Marshaller marshaller = new Jaxb2Marshaller();

	@LocalServerPort
	private int port = 0;

	@Before
	public void init() throws Exception {
		marshaller.setPackagesToScan(ClassUtils.getPackageName(BookRequest.class));
		marshaller.afterPropertiesSet();
	}

	@Test
	public void testSendRequest(){
		WebServiceTemplate webServiceTemplate = new WebServiceTemplate(marshaller);
		BookRequest bookRequest = new BookRequest();
		bookRequest.setName("Supernatural");

		Object result = webServiceTemplate.marshalSendAndReceive("http://localhost:" + port + "/ws", bookRequest);
		Assertions.assertThat(result).isNotNull();
	}

}
