package com.padippurackal.web.service;

import com.padippurackal.web.service.model.BookRequest;
import com.padippurackal.web.service.model.BookResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

@Endpoint //registers class with spring-ws for processing incoming SOAP messages.
public class BookEndpoint {

    private static final String NAMESPACE_URI = "http://web.padippurackal.com/service/model";

    @Autowired
    private BookRepository bookRepository;

    //spring-ws will pick handler method based on message's namespace & localpart.
    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "BookRequest")
    @ResponsePayload //spring-ws will map the response value to the response payload
    public BookResponse getBook(@RequestPayload BookRequest bookRequest) {
        BookResponse bookResponse = new BookResponse();
        bookResponse.setBook(bookRepository.findBook(bookRequest.getName()));

        return bookResponse;
    }
}
