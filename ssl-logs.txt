wget without passing the client certificate

wget --ca-certificate src/main/resources/jks/server-public-key.pem --post-file=src/test/resources/request.xml --header="Content-Type: text/xml" https://localhost:8443/ws -O response.xml

Client logs

    ➜  spring-soap-server git:(https-client-cert) ✗ wget --ca-certificate src/main/resources/jks/server-public-key.pem --post-file=src/test/resources/request.xml --header="Content-Type: text/xml" https://localhost:8443/ws -O response.xml
    --2018-05-29 00:54:48--  https://localhost:8443/ws
    Resolving localhost... 127.0.0.1
    Connecting to localhost|127.0.0.1|:8443... connected.
    OpenSSL: error:14094412:SSL routines:ssl3_read_bytes:sslv3 alert bad certificate
    Unable to establish SSL connection.

Server logs

    Using SSLEngineImpl.
    Allow unsafe renegotiation: false
    Allow legacy hello messages: true
    Is initial handshake: true
    Is secure renegotiation: false
    Ignoring unsupported cipher suite: TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256 for TLSv1
    Ignoring unsupported cipher suite: TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256 for TLSv1
    Ignoring unsupported cipher suite: TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA256 for TLSv1
    Ignoring unsupported cipher suite: TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA256 for TLSv1
    Ignoring unsupported cipher suite: TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256 for TLSv1.1
    Ignoring unsupported cipher suite: TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256 for TLSv1.1
    Ignoring unsupported cipher suite: TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA256 for TLSv1.1
    Ignoring unsupported cipher suite: TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA256 for TLSv1.1
    https-jsse-nio-8443-exec-1, READ: TLSv1 Handshake, length = 189
    *** ClientHello, TLSv1.2
    RandomCookie:  GMT: 1539406646 bytes = { 35, 88, 164, 54, 205, 6, 147, 35, 134, 14, 201, 235, 145, 183, 83, 2, 68, 59, 135, 211, 183, 127, 85, 72, 11, 97, 0, 14 }
    Session ID:  {}
    Cipher Suites: [TLS_ECDHE_ECDSA_WITH_AES_256_GCM_SHA384, TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384, TLS_DHE_RSA_WITH_AES_256_GCM_SHA384, Unknown 0xcc:0xa9, Unknown 0xcc:0xa8, Unknown 0xcc:0xaa, TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256, TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256, TLS_DHE_RSA_WITH_AES_128_GCM_SHA256, TLS_ECDHE_ECDSA_WITH_AES_256_CBC_SHA384, TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA384, TLS_DHE_RSA_WITH_AES_256_CBC_SHA256, TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA256, TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA256, TLS_DHE_RSA_WITH_AES_128_CBC_SHA256, TLS_ECDHE_ECDSA_WITH_AES_256_CBC_SHA, TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA, TLS_DHE_RSA_WITH_AES_256_CBC_SHA, TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA, TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA, TLS_DHE_RSA_WITH_AES_128_CBC_SHA, TLS_RSA_WITH_AES_256_GCM_SHA384, TLS_RSA_WITH_AES_128_GCM_SHA256, TLS_RSA_WITH_AES_256_CBC_SHA256, TLS_RSA_WITH_AES_128_CBC_SHA256, TLS_RSA_WITH_AES_256_CBC_SHA, TLS_RSA_WITH_AES_128_CBC_SHA, TLS_EMPTY_RENEGOTIATION_INFO_SCSV]
    Compression Methods:  { 0 }
    Extension server_name, server_name: [type=host_name (0), value=localhost]
    Extension ec_point_formats, formats: [uncompressed, ansiX962_compressed_prime, ansiX962_compressed_char2]
    Extension elliptic_curves, curve names: {unknown curve 29, secp256r1, secp521r1, secp384r1}
    Unsupported extension type_35, data:
    Unsupported extension type_22, data:
    Unsupported extension type_23, data:
    Extension signature_algorithms, signature_algorithms: SHA512withRSA, Unknown (hash:0x6, signature:0x2), SHA512withECDSA, SHA384withRSA, Unknown (hash:0x5, signature:0x2), SHA384withECDSA, SHA256withRSA, SHA256withDSA, SHA256withECDSA, SHA224withRSA, SHA224withDSA, SHA224withECDSA, SHA1withRSA, SHA1withDSA, SHA1withECDSA
    ***
    %% Initialized:  [Session-1, SSL_NULL_WITH_NULL_NULL]
    matching alias: server-keypair
    Standard ciphersuite chosen: TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256
    %% Negotiating:  [Session-1, TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256]
    *** ServerHello, TLSv1.2
    RandomCookie:  GMT: 1527485896 bytes = { 140, 128, 158, 89, 195, 59, 244, 120, 122, 122, 138, 157, 187, 118, 213, 79, 143, 107, 131, 137, 81, 91, 127, 186, 202, 200, 45, 11 }
    Session ID:  {91, 12, 150, 200, 98, 98, 143, 19, 212, 0, 32, 146, 224, 50, 163, 172, 51, 43, 128, 137, 2, 31, 192, 105, 17, 119, 142, 94, 184, 41, 87, 83}
    Cipher Suite: TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256
    Compression Method: 0
    Extension renegotiation_info, renegotiated_connection: <empty>
    ***
    Cipher suite:  TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256
    *** Certificate chain
    chain [0] = [
    [
      Version: V3
      Subject: CN=localhost, O=padippurackal.com
      Signature Algorithm: SHA256withRSA, OID = 1.2.840.113549.1.1.11

      Key:  Sun RSA public key, 2048 bits
      modulus: 19156012940872503801462796862730852393308707432305981814889011615540657828195656414095334844028223609106208209233606405071988811799712776216522667594953440826962347962362421518217388717174080995639629480019328659186190370661015754164372264539126415055881932166739181071197580309621708609118381019247893172184255209220979372345760937121153592605443368349458999823290673757754913286028350153101346667358984185752495954235003950629624891431063433155909115447033288011488796433894282889638702999664277075870015033873047094690362802343794574460328139794331749834358056386001587768739446580344113741198073106651909034516911
      public exponent: 65537
      Validity: [From: Mon May 28 22:18:10 BST 2018,
                   To: Thu May 25 22:18:10 BST 2028]
      Issuer: CN=localhost, O=padippurackal.com
      SerialNumber: [    280c77c3]

    Certificate Extensions: 1
    [1]: ObjectId: 2.5.29.14 Criticality=false
    SubjectKeyIdentifier [
    KeyIdentifier [
    0000: 8F 70 39 8F 21 57 43 EA   4F 8C 88 55 6A A2 62 29  .p9.!WC.O..Uj.b)
    0010: CA 4C F7 32                                        .L.2
    ]
    ]

    ]
      Algorithm: [SHA256withRSA]
      Signature:
    0000: 72 28 23 8B CE D3 C6 84   6E 7B 2E 35 6E A3 01 93  r(#.....n..5n...
    0010: CC 6D 1D 48 2E D9 5B 1C   DC 5A DE 7B FA B0 4B 22  .m.H..[..Z....K"
    0020: D4 7F 16 A5 9A 97 08 04   C6 E1 F8 70 4E 52 EE 8F  ...........pNR..
    0030: D4 1A 3F C0 74 DB A4 35   83 3F 83 9D FA D5 C2 CE  ..?.t..5.?......
    0040: 54 10 46 04 13 A5 23 9C   FC BD 83 96 80 E4 16 E2  T.F...#.........
    0050: 3A 06 87 9E 6B 46 F9 78   88 4F B1 22 39 B6 59 B4  :...kF.x.O."9.Y.
    0060: 5F 6F D4 AE 2C 60 A0 29   78 D7 03 28 9C 1A F9 91  _o..,`.)x..(....
    0070: 66 D2 37 04 08 3B 6C 6E   14 E8 D7 CE EA 97 EA 0D  f.7..;ln........
    0080: 37 D5 9E F1 87 9E AE F0   1A 6B CA F6 17 CB 3A 66  7........k....:f
    0090: A9 A4 D0 E6 09 CF 3C 99   44 C1 E8 64 10 7F 7C E2  ......<.D..d....
    00A0: E9 87 12 76 FF 05 D5 E0   F1 F3 CD AF AA 61 38 46  ...v.........a8F
    00B0: F5 00 D2 0F 5F B5 E3 E4   CA E9 5A 5B 23 89 C4 58  ...._.....Z[#..X
    00C0: A2 F9 60 2F 3D 48 52 11   7B 35 E0 23 95 7D F9 45  ..`/=HR..5.#...E
    00D0: F5 4B CC B1 A2 72 F0 E0   1D 95 F4 1B F0 ED 7D 98  .K...r..........
    00E0: A6 6D B2 51 6E C0 49 A4   75 2A 48 0D F1 20 1A B4  .m.Qn.I.u*H.. ..
    00F0: C4 12 51 44 B0 6E BE 48   29 E0 15 08 62 63 C3 EB  ..QD.n.H)...bc..

    ]
    ***
    *** ECDH ServerKeyExchange
    Signature Algorithm SHA512withRSA
    Server key: Sun EC public key, 256 bits
      public x coord: 28113382051072932567187749003947561874144416911926323835181514750799145848289
      public y coord: 88956475010548601326686499996737454333410601507178547616033895983629733119030
      parameters: secp256r1 [NIST P-256, X9.62 prime256v1] (1.2.840.10045.3.1.7)
    *** CertificateRequest
    Cert Types: RSA, DSS, ECDSA
    Supported Signature Algorithms: SHA512withECDSA, SHA512withRSA, SHA384withECDSA, SHA384withRSA, SHA256withECDSA, SHA256withRSA, SHA256withDSA, SHA224withECDSA, SHA224withRSA, SHA224withDSA, SHA1withECDSA, SHA1withRSA, SHA1withDSA
    Cert Authorities:
    <CN=client, O=padippurackal.com>
    *** ServerHelloDone
    https-jsse-nio-8443-exec-1, WRITE: TLSv1.2 Handshake, length = 1286
    https-jsse-nio-8443-exec-2, READ: TLSv1.2 Handshake, length = 7
    *** Certificate chain
    <Empty>
    ***
    https-jsse-nio-8443-exec-2, fatal error: 42: null cert chain
    javax.net.ssl.SSLHandshakeException: null cert chain
    %% Invalidated:  [Session-1, TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256]
    https-jsse-nio-8443-exec-2, SEND TLSv1.2 ALERT:  fatal, description = bad_certificate
    https-jsse-nio-8443-exec-2, WRITE: TLSv1.2 Alert, length = 2
    https-jsse-nio-8443-exec-2, fatal: engine already closed.  Rethrowing javax.net.ssl.SSLHandshakeException: null cert chain
    https-jsse-nio-8443-exec-2, called closeOutbound()
    https-jsse-nio-8443-exec-2, closeOutboundInternal()


wget with client certificate - client.p12 -> contains both public & private keys
wget --ca-certificate src/main/resources/jks/server-public-key.pem --certificate src/main/resources/jks/client.p12 --post-file=src/test/resources/request.xml --header="Content-Type: text/xml" https://localhost:8443/ws -O response.xml

Client Logs

    ➜  spring-soap-server git:(https-client-cert) ✗ wget --ca-certificate src/main/resources/jks/server-public-key.pem --certificate src/main/resources/jks/client.p12 --post-file=src/test/resources/request.xml --header="Content-Type: text/xml" https://localhost:8443/ws -O response.xml
    --2018-05-29 00:56:41--  https://localhost:8443/ws
    Resolving localhost... 127.0.0.1
    Connecting to localhost|127.0.0.1|:8443... connected.
    HTTP request sent, awaiting response... 200
    Length: 410 [text/xml]
    Saving to: 'response.xml'

    response.xml                                       100%[================================================================================================================>]     410  --.-KB/s    in 0s

    2018-05-29 00:56:41 (39.1 MB/s) - 'response.xml' saved [410/410]

    ➜  spring-soap-server git:(https-client-cert) ✗

Server Logs

    Using SSLEngineImpl.
    Allow unsafe renegotiation: false
    Allow legacy hello messages: true
    Is initial handshake: true
    Is secure renegotiation: false
    Ignoring unsupported cipher suite: TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256 for TLSv1
    Ignoring unsupported cipher suite: TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256 for TLSv1
    Ignoring unsupported cipher suite: TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA256 for TLSv1
    Ignoring unsupported cipher suite: TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA256 for TLSv1
    Ignoring unsupported cipher suite: TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256 for TLSv1.1
    Ignoring unsupported cipher suite: TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256 for TLSv1.1
    Ignoring unsupported cipher suite: TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA256 for TLSv1.1
    Ignoring unsupported cipher suite: TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA256 for TLSv1.1
    https-jsse-nio-8443-exec-3, READ: TLSv1 Handshake, length = 189
    *** ClientHello, TLSv1.2
    RandomCookie:  GMT: -170499321 bytes = { 138, 183, 83, 182, 83, 177, 102, 115, 119, 204, 75, 249, 209, 100, 130, 209, 153, 223, 16, 221, 102, 104, 250, 216, 148, 225, 223, 10 }
    Session ID:  {}
    Cipher Suites: [TLS_ECDHE_ECDSA_WITH_AES_256_GCM_SHA384, TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384, TLS_DHE_RSA_WITH_AES_256_GCM_SHA384, Unknown 0xcc:0xa9, Unknown 0xcc:0xa8, Unknown 0xcc:0xaa, TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256, TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256, TLS_DHE_RSA_WITH_AES_128_GCM_SHA256, TLS_ECDHE_ECDSA_WITH_AES_256_CBC_SHA384, TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA384, TLS_DHE_RSA_WITH_AES_256_CBC_SHA256, TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA256, TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA256, TLS_DHE_RSA_WITH_AES_128_CBC_SHA256, TLS_ECDHE_ECDSA_WITH_AES_256_CBC_SHA, TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA, TLS_DHE_RSA_WITH_AES_256_CBC_SHA, TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA, TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA, TLS_DHE_RSA_WITH_AES_128_CBC_SHA, TLS_RSA_WITH_AES_256_GCM_SHA384, TLS_RSA_WITH_AES_128_GCM_SHA256, TLS_RSA_WITH_AES_256_CBC_SHA256, TLS_RSA_WITH_AES_128_CBC_SHA256, TLS_RSA_WITH_AES_256_CBC_SHA, TLS_RSA_WITH_AES_128_CBC_SHA, TLS_EMPTY_RENEGOTIATION_INFO_SCSV]
    Compression Methods:  { 0 }
    Extension server_name, server_name: [type=host_name (0), value=localhost]
    Extension ec_point_formats, formats: [uncompressed, ansiX962_compressed_prime, ansiX962_compressed_char2]
    Extension elliptic_curves, curve names: {unknown curve 29, secp256r1, secp521r1, secp384r1}
    Unsupported extension type_35, data:
    Unsupported extension type_22, data:
    Unsupported extension type_23, data:
    Extension signature_algorithms, signature_algorithms: SHA512withRSA, Unknown (hash:0x6, signature:0x2), SHA512withECDSA, SHA384withRSA, Unknown (hash:0x5, signature:0x2), SHA384withECDSA, SHA256withRSA, SHA256withDSA, SHA256withECDSA, SHA224withRSA, SHA224withDSA, SHA224withECDSA, SHA1withRSA, SHA1withDSA, SHA1withECDSA
    ***
    %% Initialized:  [Session-2, SSL_NULL_WITH_NULL_NULL]
    Standard ciphersuite chosen: TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256
    %% Negotiating:  [Session-2, TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256]
    *** ServerHello, TLSv1.2
    RandomCookie:  GMT: 1527486265 bytes = { 193, 23, 250, 62, 24, 170, 98, 150, 197, 159, 245, 34, 85, 161, 191, 86, 207, 198, 113, 184, 213, 62, 103, 87, 1, 38, 20, 223 }
    Session ID:  {91, 12, 151, 57, 114, 166, 177, 181, 44, 162, 148, 217, 231, 219, 254, 224, 144, 153, 82, 168, 235, 94, 186, 103, 145, 250, 35, 55, 63, 31, 255, 215}
    Cipher Suite: TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256
    Compression Method: 0
    Extension renegotiation_info, renegotiated_connection: <empty>
    ***
    Cipher suite:  TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256
    *** Certificate chain
    chain [0] = [
    [
      Version: V3
      Subject: CN=localhost, O=padippurackal.com
      Signature Algorithm: SHA256withRSA, OID = 1.2.840.113549.1.1.11

      Key:  Sun RSA public key, 2048 bits
      modulus: 19156012940872503801462796862730852393308707432305981814889011615540657828195656414095334844028223609106208209233606405071988811799712776216522667594953440826962347962362421518217388717174080995639629480019328659186190370661015754164372264539126415055881932166739181071197580309621708609118381019247893172184255209220979372345760937121153592605443368349458999823290673757754913286028350153101346667358984185752495954235003950629624891431063433155909115447033288011488796433894282889638702999664277075870015033873047094690362802343794574460328139794331749834358056386001587768739446580344113741198073106651909034516911
      public exponent: 65537
      Validity: [From: Mon May 28 22:18:10 BST 2018,
                   To: Thu May 25 22:18:10 BST 2028]
      Issuer: CN=localhost, O=padippurackal.com
      SerialNumber: [    280c77c3]

    Certificate Extensions: 1
    [1]: ObjectId: 2.5.29.14 Criticality=false
    SubjectKeyIdentifier [
    KeyIdentifier [
    0000: 8F 70 39 8F 21 57 43 EA   4F 8C 88 55 6A A2 62 29  .p9.!WC.O..Uj.b)
    0010: CA 4C F7 32                                        .L.2
    ]
    ]

    ]
      Algorithm: [SHA256withRSA]
      Signature:
    0000: 72 28 23 8B CE D3 C6 84   6E 7B 2E 35 6E A3 01 93  r(#.....n..5n...
    0010: CC 6D 1D 48 2E D9 5B 1C   DC 5A DE 7B FA B0 4B 22  .m.H..[..Z....K"
    0020: D4 7F 16 A5 9A 97 08 04   C6 E1 F8 70 4E 52 EE 8F  ...........pNR..
    0030: D4 1A 3F C0 74 DB A4 35   83 3F 83 9D FA D5 C2 CE  ..?.t..5.?......
    0040: 54 10 46 04 13 A5 23 9C   FC BD 83 96 80 E4 16 E2  T.F...#.........
    0050: 3A 06 87 9E 6B 46 F9 78   88 4F B1 22 39 B6 59 B4  :...kF.x.O."9.Y.
    0060: 5F 6F D4 AE 2C 60 A0 29   78 D7 03 28 9C 1A F9 91  _o..,`.)x..(....
    0070: 66 D2 37 04 08 3B 6C 6E   14 E8 D7 CE EA 97 EA 0D  f.7..;ln........
    0080: 37 D5 9E F1 87 9E AE F0   1A 6B CA F6 17 CB 3A 66  7........k....:f
    0090: A9 A4 D0 E6 09 CF 3C 99   44 C1 E8 64 10 7F 7C E2  ......<.D..d....
    00A0: E9 87 12 76 FF 05 D5 E0   F1 F3 CD AF AA 61 38 46  ...v.........a8F
    00B0: F5 00 D2 0F 5F B5 E3 E4   CA E9 5A 5B 23 89 C4 58  ...._.....Z[#..X
    00C0: A2 F9 60 2F 3D 48 52 11   7B 35 E0 23 95 7D F9 45  ..`/=HR..5.#...E
    00D0: F5 4B CC B1 A2 72 F0 E0   1D 95 F4 1B F0 ED 7D 98  .K...r..........
    00E0: A6 6D B2 51 6E C0 49 A4   75 2A 48 0D F1 20 1A B4  .m.Qn.I.u*H.. ..
    00F0: C4 12 51 44 B0 6E BE 48   29 E0 15 08 62 63 C3 EB  ..QD.n.H)...bc..

    ]
    ***
    *** ECDH ServerKeyExchange
    Signature Algorithm SHA512withRSA
    Server key: Sun EC public key, 256 bits
      public x coord: 67748305003664319858749321923063282604788355812015040187228786960866606984751
      public y coord: 83180892654863291966916537129413837172206777547922362875018247203560913044059
      parameters: secp256r1 [NIST P-256, X9.62 prime256v1] (1.2.840.10045.3.1.7)
    *** CertificateRequest
    Cert Types: RSA, DSS, ECDSA
    Supported Signature Algorithms: SHA512withECDSA, SHA512withRSA, SHA384withECDSA, SHA384withRSA, SHA256withECDSA, SHA256withRSA, SHA256withDSA, SHA224withECDSA, SHA224withRSA, SHA224withDSA, SHA1withECDSA, SHA1withRSA, SHA1withDSA
    Cert Authorities:
    <CN=client, O=padippurackal.com>
    *** ServerHelloDone
    https-jsse-nio-8443-exec-3, WRITE: TLSv1.2 Handshake, length = 1286
    https-jsse-nio-8443-exec-4, READ: TLSv1.2 Handshake, length = 775
    *** Certificate chain
    chain [0] = [
    [
      Version: V3
      Subject: CN=client, O=padippurackal.com
      Signature Algorithm: SHA256withRSA, OID = 1.2.840.113549.1.1.11

      Key:  Sun RSA public key, 2048 bits
      modulus: 26406318306187059467646637956081341608282969568696201999725963819644372805572585136628305086500821569236131804449026120787040702783625694612333716575893993227018299655834090047069584251516845907514795660395936839018680683308830124608768374737834252373121949738482408014027568025400915201165054930223578650413784151042634826672587323641687494742451130466084984973440532096457732325394344022338397381926334486847391701070997992405867037646963546822256471792909388724461678880230620504231632552869259332874549042422146324468326516531959045902571953032465707130352055920075118875348871778940590515350943468267875751267383
      public exponent: 65537
      Validity: [From: Mon May 28 22:42:05 BST 2018,
                   To: Thu May 25 22:42:05 BST 2028]
      Issuer: CN=client, O=padippurackal.com
      SerialNumber: [    2477c300]

    Certificate Extensions: 1
    [1]: ObjectId: 2.5.29.14 Criticality=false
    SubjectKeyIdentifier [
    KeyIdentifier [
    0000: 7B 00 CA 68 0B 45 7A 4C   77 12 FE 9B A1 1D 5A 21  ...h.EzLw.....Z!
    0010: B2 FE AE F7                                        ....
    ]
    ]

    ]
      Algorithm: [SHA256withRSA]
      Signature:
    0000: 29 B8 E2 E4 0C 04 CD 97   ED 75 68 4C 35 DB CB 70  )........uhL5..p
    0010: BD 69 97 7E 85 EB 3D AD   94 32 E3 2D 05 27 BE 1A  .i....=..2.-.'..
    0020: FB F4 56 94 36 05 84 11   2E 30 0B 62 F5 CD 14 23  ..V.6....0.b...#
    0030: 08 73 05 5F A0 B3 A1 B1   DE 54 84 B8 99 21 17 65  .s._.....T...!.e
    0040: 27 2C 12 2A 06 B0 4E 7D   E1 1A DA 44 56 5B FD 90  ',.*..N....DV[..
    0050: E9 2F 84 5F 99 08 AE 48   9E 20 CB A3 7C 45 61 C8  ./._...H. ...Ea.
    0060: 57 49 2F B4 1F 0B D1 D9   38 DD 6C B5 FE D4 DC 1B  WI/.....8.l.....
    0070: 7B AA 45 ED 1A 17 5A DD   B6 90 51 86 C0 73 13 6E  ..E...Z...Q..s.n
    0080: D2 B3 F9 EF 74 3C 0F A9   FA BD F7 80 16 C4 4A C4  ....t<........J.
    0090: 56 14 05 77 C8 AB 68 76   9A 72 8F A0 72 ED CC BD  V..w..hv.r..r...
    00A0: 22 67 E7 FB 1F 3C 27 13   F2 B1 90 93 C9 08 BD 31  "g...<'........1
    00B0: 0E 51 74 57 22 2E 68 11   66 2E A3 61 2A 2C DD 2D  .QtW".h.f..a*,.-
    00C0: BD 6E DB C5 B2 59 76 4B   70 E0 38 65 EC A9 EF 93  .n...YvKp.8e....
    00D0: 72 C7 43 0A E8 31 FE 33   65 CB 21 38 92 6A D3 77  r.C..1.3e.!8.j.w
    00E0: B6 3B 1A 02 37 4E 94 9A   47 B6 CF 10 62 FB 3E 68  .;..7N..G...b.>h
    00F0: 33 E0 6C 0A 42 03 D1 B0   89 03 2A 32 7F E0 54 8F  3.l.B.....*2..T.

    ]
    ***
    Found trusted certificate:
    [
    [
      Version: V3
      Subject: CN=client, O=padippurackal.com
      Signature Algorithm: SHA256withRSA, OID = 1.2.840.113549.1.1.11

      Key:  Sun RSA public key, 2048 bits
      modulus: 26406318306187059467646637956081341608282969568696201999725963819644372805572585136628305086500821569236131804449026120787040702783625694612333716575893993227018299655834090047069584251516845907514795660395936839018680683308830124608768374737834252373121949738482408014027568025400915201165054930223578650413784151042634826672587323641687494742451130466084984973440532096457732325394344022338397381926334486847391701070997992405867037646963546822256471792909388724461678880230620504231632552869259332874549042422146324468326516531959045902571953032465707130352055920075118875348871778940590515350943468267875751267383
      public exponent: 65537
      Validity: [From: Mon May 28 22:42:05 BST 2018,
                   To: Thu May 25 22:42:05 BST 2028]
      Issuer: CN=client, O=padippurackal.com
      SerialNumber: [    2477c300]

    Certificate Extensions: 1
    [1]: ObjectId: 2.5.29.14 Criticality=false
    SubjectKeyIdentifier [
    KeyIdentifier [
    0000: 7B 00 CA 68 0B 45 7A 4C   77 12 FE 9B A1 1D 5A 21  ...h.EzLw.....Z!
    0010: B2 FE AE F7                                        ....
    ]
    ]

    ]
      Algorithm: [SHA256withRSA]
      Signature:
    0000: 29 B8 E2 E4 0C 04 CD 97   ED 75 68 4C 35 DB CB 70  )........uhL5..p
    0010: BD 69 97 7E 85 EB 3D AD   94 32 E3 2D 05 27 BE 1A  .i....=..2.-.'..
    0020: FB F4 56 94 36 05 84 11   2E 30 0B 62 F5 CD 14 23  ..V.6....0.b...#
    0030: 08 73 05 5F A0 B3 A1 B1   DE 54 84 B8 99 21 17 65  .s._.....T...!.e
    0040: 27 2C 12 2A 06 B0 4E 7D   E1 1A DA 44 56 5B FD 90  ',.*..N....DV[..
    0050: E9 2F 84 5F 99 08 AE 48   9E 20 CB A3 7C 45 61 C8  ./._...H. ...Ea.
    0060: 57 49 2F B4 1F 0B D1 D9   38 DD 6C B5 FE D4 DC 1B  WI/.....8.l.....
    0070: 7B AA 45 ED 1A 17 5A DD   B6 90 51 86 C0 73 13 6E  ..E...Z...Q..s.n
    0080: D2 B3 F9 EF 74 3C 0F A9   FA BD F7 80 16 C4 4A C4  ....t<........J.
    0090: 56 14 05 77 C8 AB 68 76   9A 72 8F A0 72 ED CC BD  V..w..hv.r..r...
    00A0: 22 67 E7 FB 1F 3C 27 13   F2 B1 90 93 C9 08 BD 31  "g...<'........1
    00B0: 0E 51 74 57 22 2E 68 11   66 2E A3 61 2A 2C DD 2D  .QtW".h.f..a*,.-
    00C0: BD 6E DB C5 B2 59 76 4B   70 E0 38 65 EC A9 EF 93  .n...YvKp.8e....
    00D0: 72 C7 43 0A E8 31 FE 33   65 CB 21 38 92 6A D3 77  r.C..1.3e.!8.j.w
    00E0: B6 3B 1A 02 37 4E 94 9A   47 B6 CF 10 62 FB 3E 68  .;..7N..G...b.>h
    00F0: 33 E0 6C 0A 42 03 D1 B0   89 03 2A 32 7F E0 54 8F  3.l.B.....*2..T.

    ]
    https-jsse-nio-8443-exec-4, READ: TLSv1.2 Handshake, length = 70
    *** ECDHClientKeyExchange
    ECDH Public value:  { 4, 30, 130, 114, 225, 108, 68, 229, 50, 153, 116, 132, 86, 161, 239, 145, 6, 250, 9, 150, 112, 13, 98, 148, 13, 125, 229, 147, 54, 225, 164, 158, 124, 233, 221, 105, 241, 89, 0, 193, 164, 86, 200, 121, 20, 233, 249, 56, 99, 103, 208, 1, 214, 199, 87, 238, 173, 42, 216, 82, 122, 165, 117, 41, 238 }
    SESSION KEYGEN:
    PreMaster Secret:
    0000: 8A 84 78 BA 90 A0 D2 93   9D AE 98 55 A1 32 92 D6  ..x........U.2..
    0010: F7 5D C5 E1 7D B5 92 FC   69 7D ED 01 C1 AF C1 D7  .]......i.......
    CONNECTION KEYGEN:
    Client Nonce:
    0000: F6 D6 63 07 8A B7 53 B6   53 B1 66 73 77 CC 4B F9  ..c...S.S.fsw.K.
    0010: D1 64 82 D1 99 DF 10 DD   66 68 FA D8 94 E1 DF 0A  .d......fh......
    Server Nonce:
    0000: 5B 0C 97 39 C1 17 FA 3E   18 AA 62 96 C5 9F F5 22  [..9...>..b...."
    0010: 55 A1 BF 56 CF C6 71 B8   D5 3E 67 57 01 26 14 DF  U..V..q..>gW.&..
    Master Secret:
    0000: 10 C7 D6 DE FF 7B 9F 24   53 5C 22 54 91 17 67 B6  .......$S\"T..g.
    0010: FF 16 55 89 A8 64 CA D4   ED B8 04 27 8E 28 95 0A  ..U..d.....'.(..
    0020: 41 80 E6 72 3E D0 8A 8B   EF C8 B3 99 02 54 36 4C  A..r>........T6L
    ... no MAC keys used for this cipher
    Client write key:
    0000: 70 B0 5E DC 2E AF 04 AE   16 58 0F 53 1B 8B 0D 85  p.^......X.S....
    Server write key:
    0000: 8C 92 F7 D5 88 AE 72 64   E8 9E 70 AD 6D 8E 1A 60  ......rd..p.m..`
    Client write IV:
    0000: E9 FB 72 4D                                        ..rM
    Server write IV:
    0000: 86 EC 34 DC                                        ..4.
    https-jsse-nio-8443-exec-4, READ: TLSv1.2 Handshake, length = 264
    *** CertificateVerify
    Signature Algorithm SHA512withRSA
    https-jsse-nio-8443-exec-4, READ: TLSv1.2 Change Cipher Spec, length = 1
    https-jsse-nio-8443-exec-4, READ: TLSv1.2 Handshake, length = 40
    *** Finished
    verify_data:  { 202, 18, 223, 225, 63, 194, 32, 90, 95, 101, 103, 49 }
    ***
    https-jsse-nio-8443-exec-4, WRITE: TLSv1.2 Change Cipher Spec, length = 1
    *** Finished
    verify_data:  { 28, 237, 207, 192, 86, 214, 201, 47, 42, 31, 138, 222 }
    ***
    https-jsse-nio-8443-exec-4, WRITE: TLSv1.2 Handshake, length = 40
    %% Cached server session: [Session-2, TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256]
    2018-05-29 00:56:41.542  INFO 46017 --- [nio-8443-exec-6] o.a.c.c.C.[Tomcat].[localhost].[/]       : Initializing Spring FrameworkServlet 'messageDispatcherServlet'
    2018-05-29 00:56:41.542  INFO 46017 --- [nio-8443-exec-6] o.s.w.t.http.MessageDispatcherServlet    : FrameworkServlet 'messageDispatcherServlet': initialization started
    2018-05-29 00:56:41.544  INFO 46017 --- [nio-8443-exec-6] o.s.ws.soap.saaj.SaajSoapMessageFactory  : Creating SAAJ 1.3 MessageFactory with SOAP 1.1 Protocol
    2018-05-29 00:56:41.551  INFO 46017 --- [nio-8443-exec-6] o.s.w.t.http.MessageDispatcherServlet    : FrameworkServlet 'messageDispatcherServlet': initialization completed in 8 ms
    https-jsse-nio-8443-exec-6, WRITE: TLSv1.2 Application Data, length = 611
    https-jsse-nio-8443-exec-6, called closeOutbound()
    https-jsse-nio-8443-exec-6, closeOutboundInternal()
    https-jsse-nio-8443-exec-6, SEND TLSv1.2 ALERT:  warning, description = close_notify
    https-jsse-nio-8443-exec-6, WRITE: TLSv1.2 Alert, length = 26
