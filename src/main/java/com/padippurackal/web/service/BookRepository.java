package com.padippurackal.web.service;


import com.padippurackal.web.service.model.Book;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;

@Component
public class BookRepository {
    private static final Map<String, Book> books = new HashMap<>();

    @PostConstruct
    public void initData() {
        Book supernatural = new Book();
        supernatural.setName("Supernatural");
        supernatural.setAuthor("Graham Hancock");
        supernatural.setIsbn("1231231234");
        supernatural.setPublisher("Packt Publishing");

        books.put(supernatural.getName(), supernatural);
    }

    public Book findBook(String name){
        Assert.notNull(name, "The book's name must not be null");
        return books.get(name);
    }
}
